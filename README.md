# The MonoRepo

1. Keep all personal development in one place.
2. Make it easy to reference different projects.
3. Make it easy to automate everything.
